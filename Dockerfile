FROM python:3 AS builder
ADD . /opt/fetch-paper-api
WORKDIR /opt/fetch-paper-api
RUN pip install --no-cache-dir -r requirements.txt

ARG PROJECT='paper'
ARG VERSION='1.18.1'

COPY main.py ./fetch.py
RUN python fetch.py $PROJECT $VERSION
